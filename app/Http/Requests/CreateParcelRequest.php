<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reference' => 'nullable|string|min:5|max:512',
            'recipient_first_name' => 'required|string|min:3|max:64',
            'recipient_last_name' => 'required|string|min:3|max:64',
            'recipient_street' => 'required|string|min:3|max:64',
            'recipient_number' => 'required|numeric',
            'recipient_number_suffix' => 'required|string|min:2|max:64',
            'recipient_postal_code' => 'required|string|min:2|max:64',
            'recipient_city' => 'required|string|min:2|max:64',
            'recipient_cc' => 'required|string|min:2|max:64',
            'sender_company_name' => 'required|string|min:3|max:64',
            'sender_line_address' => 'required|string|min:3|max:64',
            'sender_street' => 'required|string|min:3|max:64',
            'sender_number' => 'required|numeric',
            'sender_postal_code' => 'required|string|min:2|max:64',
            'sender_city' => 'required|string|min:2|max:64',
            'sender_cc' => 'required|string|min:2|max:64',
            'pieces' => 'nullable|array',
        ];
    }
}
