<?php

namespace App\Http\Controllers\Api;

use bawes\myfatoorah\MyFatoorah;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $my;

    public function __construct()
    {
        $merchantCode = config('fatoorah.MerchantCode');
        $username = config('fatoorah.MerchantUsername');
        $password = config('fatoorah.MerchantPassword');
        $this->my = MyFatoorah::live($merchantCode, $username, $password);
    }

    public function status(Request $request)
    {
        // Example Ref ID
        $myfatoorahRefId = $request->order_id;

        $orderStatus = null;

        try {
            // Order status on Test environment
            $orderStatus = $this->my->getOrderStatus($myfatoorahRefId);
        } catch (Exception $exp) {
            return response()->json([
                'success' => false,
                'data' => $exp->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'data' => $orderStatus
        ]);
    }

    public function pay(Request $request)
    {
        $cart = []; // get cart items

        $this->my->setPaymentMode(MyFatoorah::GATEWAY_ALL)
            ->setReturnUrl("https://google.com")
            ->setErrorReturnUrl("https://google.com")
            ->setCustomer(
                $request->customer_username,
                $request->customer_email,
                $request->customer_email
            )->setReferenceId(); //Pass unique order number or leave empty to use time()

        foreach($cart as $item) {
            $this->my->addProduct($item->name, $item->price, $item->quantity);
        }

        try {
            // Order status on Test environment
            $this->my->getPaymentLinkAndReference();
        } catch (Exception $exp) {
            return response()->json([
                'success' => false,
                'data' => $exp->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'data' => [
                'payment_url' => $this->my['paymentUrl'],
                'payment_ref' => $this->my['paymentRef']
            ]
        ]);
    }
}
