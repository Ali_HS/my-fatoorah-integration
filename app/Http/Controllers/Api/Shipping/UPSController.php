<?php

namespace App\Http\Controllers\Api\Shipping;

use Ups\Shipping;
use Ups\Entity\Package;
use Ups\Entity\Shipment;
use Ups\Entity\ReferenceNumber;
use Ups\Entity\PaymentInformation;
use Ups\Entity\RateInformation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UPSController extends Controller
{
    public function createshipment(Request $request)
    {
        $shipment = new Shipment;
        // Set shipper
        $shipment = $this->shipper($shipment);

        // To address
        $shipment = $this->to($shipment);

        // From address
        $shipment = $this->from($shipment);

        // Sold to
        $shipment = $this->sold($shipment);

        // Set service
        $shipment = $this->service($shipment);

        // Mark as a return (if return)
        // if ($return) {
        //     $returnService = new \Ups\Entity\ReturnService;
        //     $returnService->setCode(\Ups\Entity\ReturnService::PRINT_RETURN_LABEL_PRL);
        //     $shipment->setReturnService($returnService);
        // }

        // Set description
        $shipment->setDescription('XX');

        // create Package
        $package = new Package;
        $package = $this->package($package);

        // Add this package
        $shipment->addPackage($package);

        // Set Reference Number
        $referenceNumber = new ReferenceNumber;
        $referenceNumber = $this->referenceNumber($referenceNumber);
        $shipment->setReferenceNumber($referenceNumber);

        // Set payment information
        $payment = $this->payment();
        $shipment->setPaymentInformation($payment);

        // Ask for negotiated rates (optional)
        $rateInformation = new RateInformation;
        $rateInformation->setNegotiatedRatesIndicator(1);
        $shipment->setRateInformation($rateInformation);
        // Get shipment info
        try {
            $accessKey = config('ups.AccessKey');
            $userId = config('ups.UserId');
            $password = config('ups.Password');
            $api = new Shipping($accessKey, $userId, $password);

            $confirm = $api->confirm(Shipping::REQ_VALIDATE, $shipment);
            if ($confirm) {
                $accept = $api->accept($confirm->ShipmentDigest);
                return response()->json(['success' => true, 'data' => $accept]);
            }
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()]);
        }
    }

    private function shipper(Shipment $shipment)
    {
        $shipper = $shipment->getShipper();
        $shipper->setShipperNumber('XX');
        $shipper->setName('XX');
        $shipper->setAttentionName('XX');
        $shipperAddress = $shipper->getAddress();
        $shipperAddress->setAddressLine1('XX');
        $shipperAddress->setPostalCode('XX');
        $shipperAddress->setCity('XX');
        $shipperAddress->setStateProvinceCode('XX'); // required in US
        $shipperAddress->setCountryCode('XX');
        $shipper->setAddress($shipperAddress);
        $shipper->setEmailAddress('XX');
        $shipper->setPhoneNumber('XX');
        $shipment->setShipper($shipper);
        return $shipment;
    }

    private function to(Shipment $shipment)
    {
        $address = new \Ups\Entity\Address();
        $address->setAddressLine1('XX');
        $address->setPostalCode('XX');
        $address->setCity('XX');
        $address->setStateProvinceCode('XX');  // Required in US
        $address->setCountryCode('XX');
        $shipTo = new \Ups\Entity\ShipTo();
        $shipTo->setAddress($address);
        $shipTo->setCompanyName('XX');
        $shipTo->setAttentionName('XX');
        $shipTo->setEmailAddress('XX');
        $shipTo->setPhoneNumber('XX');
        $shipment->setShipTo($shipTo);
        return $shipment;
    }

    private function from(Shipment $shipment)
    {
        $address = new \Ups\Entity\Address();
        $address->setAddressLine1('XX');
        $address->setPostalCode('XX');
        $address->setCity('XX');
        $address->setStateProvinceCode('XX');
        $address->setCountryCode('XX');
        $shipFrom = new \Ups\Entity\ShipFrom();
        $shipFrom->setAddress($address);
        $shipFrom->setName('XX');
        $shipFrom->setAttentionName($shipFrom->getName());
        $shipFrom->setCompanyName($shipFrom->getName());
        $shipFrom->setEmailAddress('XX');
        $shipFrom->setPhoneNumber('XX');
        $shipment->setShipFrom($shipFrom);
        return $shipment;
    }

    private function sold(Shipment $shipment)
    {
        $address = new \Ups\Entity\Address();
        $address->setAddressLine1('XX');
        $address->setPostalCode('XX');
        $address->setCity('XX');
        $address->setCountryCode('XX');
        $address->setStateProvinceCode('XX');
        $soldTo = new \Ups\Entity\SoldTo;
        $soldTo->setAddress($address);
        $soldTo->setAttentionName('XX');
        $soldTo->setCompanyName($soldTo->getAttentionName());
        $soldTo->setEmailAddress('XX');
        $soldTo->setPhoneNumber('XX');
        $shipment->setSoldTo($soldTo);
        return $shipment;
    }

    private function service(Shipment $shipment)
    {
        $service = new \Ups\Entity\Service;
        $service->setCode(\Ups\Entity\Service::S_STANDARD);
        $service->setDescription($service->getName());
        $shipment->setService($service);
        return $shipment;
    }

    private function package(Package $package)
    {
        $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
        $package->getPackageWeight()->setWeight(10);
        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
        $package->getPackageWeight()->setUnitOfMeasurement($unit);

        // Set Package Service Options
        $packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
        $packageServiceOptions->setShipperReleaseIndicator(true);
        $package->setPackageServiceOptions($packageServiceOptions);

        // Set dimensions
        $dimensions = new \Ups\Entity\Dimensions();
        $dimensions->setHeight(50);
        $dimensions->setWidth(50);
        $dimensions->setLength(50);
        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
        $dimensions->setUnitOfMeasurement($unit);
        $package->setDimensions($dimensions);

        // Add descriptions because it is a package
        $package->setDescription('XX');
        return $package;
    }

    private function referenceNumber(ReferenceNumber $referenceNumber)
    {
        $return = true;
        $return_id = rand(); // must be valid return id
        $order_id = rand(); // must be valid order id
        if ($return) {
            $referenceNumber = $this->shipmentReturn($referenceNumber, $return_id);
        } else {
            $referenceNumber = $this->shipmentOrder($referenceNumber, $order_id);
        }
        return $referenceNumber;
    }

    private function shipmentReturn($referenceNumber, $return_id)
    {
        $referenceNumber->setCode(ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
        $referenceNumber->setValue($return_id);
        return $referenceNumber;
    }

    private function shipmentOrder($referenceNumber, $order_id)
    {
        $referenceNumber->setCode(ReferenceNumber::CODE_INVOICE_NUMBER);
        $referenceNumber->setValue($order_id);
        return $referenceNumber;
    }

    private function payment()
    {
        $accountNumber = (object)[
            'AccountNumber' => rand() // must be valid
        ];
        $type = 'prepaid';
        return new PaymentInformation($type, $accountNumber);
    }
}
