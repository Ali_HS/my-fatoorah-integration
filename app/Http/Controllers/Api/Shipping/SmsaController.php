<?php

namespace App\Http\Controllers\api\shipping;

use SmsaSDK\Smsa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SmsaController extends Controller
{
    public function createshipment(Request $request)
    {
        $key = config('smsa.PassKey');
        Smsa::key($key);
        Smsa::nullValues('');
        $shipmentData = $this->shipment();
        $shipment = Smsa::addShipment($shipmentData);
        $awbNumber = $shipment->getAddShipmentResult();
        $status = Smsa::getStatus(['awbNo' => $awbNumber])->getGetStatusResult();
        return response()->json(
            [
                'success' => true,
                'data' => [
                    'shipment awb' => $awbNumber,
                    'shipment status' => $status
                ]
            ]
        );
    }

    private function shipment() {
        return [
            'refNo' => 'my_app_name' . time(), // shipment reference in your application
            'cName' => 'Mohannad Najjar', // customer name
            'cntry' => 'SA', // shipment country
            'cCity' => 'JEDDAH', // shipment city, try: Smsa::getRTLCities() to get the supported cities
            'cMobile' => '0555555555', // customer mobile
            'cAddr1' => 'ALNAHDA DIST, ...detailed address here', // customer address
            'cAddr2' => 'ALBAWADI DIST, ...detailed address here', // customer address 2
            'shipType' => 'DLV', // shipment type
            'PCs' => 1, // quantity of the shipped pieces
            'cEmail' => 'mohannad.najjar@mail.com', // customer email
            'codAmt' => '50', // payment amount if it's cash on delivery, 0 if not cash on delivery
            'weight' => '10', // pieces weight
            'itemDesc' => 'Foo Bar', // extra description will be printed
        ];
    }
}
