<?php

namespace App\Http\Controllers\Api\Shipping;

use Illuminate\Http\Request;
use Mvdnbrk\DhlParcel\Client;
use App\Http\Controllers\Controller;
use Mvdnbrk\Laravel\Facades\DhlParcel;
use App\Http\Requests\CreateParcelRequest;

class DhlController extends Controller
{
    private $parcel;

    public function __construct(Client $client)
    {
        $this->parcel = $client;
        $this->parcel->setUserId(config('dhl.DhlParcelUserId'));
        $this->parcel->setApiKey(config('dhl.DhlParcelApiKey'));
    }

    public function createshipment(CreateParcelRequest $request) {
        $parcel = $this->createParcel($request);
        $shipment = DhlParcel::shipments()->create($parcel);
        return response()->json(['success' => true, 'data' => $shipment]);
    }

    private function createParcel(Request $request) {
        $pieces = [];
        if($request->has('pieces') && is_array($request->pieces)) {
            foreach ($request->pieces as $piece) {
                $size = '';
                $dhl_piece = [
                    'parcel_type' => $piece->size,
                    'quantity' => $piece->quantity
                ];
                array_push($pieces, $dhl_piece);
            }
        }
        $parcel = [];
        $parcel['reference'] = $request->reference;
        $parcel['recipient'] = [
            'first_name' => $request->recipient_first_name,
            'last_name' => $request->recipient_last_name,
            'street' => $request->recipient_street,
            'number' => $request->recipient_number,
            'number_suffix' => $request->recipient_number_suffix,
            'postal_code' => $request->recipient_postal_code,
            'city' => $request->recipient_city,
            'cc' => $request->recipient_cc,
        ];
        $parcel['sender'] = [
            'company_name' => $request->sender_company_name,
            'street' => $request->sender_street,
            'additional_address_line' => $request->sender_address_line,
            'number' => $request->sender_number,
            'postal_code' => $request->sender_postal_code,
            'city' => $request->sender_city,
            'cc' => $request->sender_cc,
        ];
        if(count($pieces)) {
            $parcel['pieces'] = $pieces;
        }
        return $parcel;
    }
}
