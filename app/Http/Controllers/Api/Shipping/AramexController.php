<?php

namespace App\Http\Controllers\Api\Shipping;
use Octw\Aramex\Aramex;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AramexController extends Controller
{

    //////// go to confing/aramex.php and put your configurations there.  ///////////

    public function createpickup(Request $request) {

        // validate addresses and skipping struggling with users' inputs
        // thats not compatible with Aramex's end.
        $validateAddress = Aramex::validateAddress([
            'line_1'=> $request->line1, // optional (Passing it is recommended)
            'line_2'=> $request->line2, // optional
            'line_3'=> $request->line3, // optional
            'country_code' => $request->country_code,
            'city' => $request->city,
        ]);
        if (!$validateAddress->HasErrors) {
            $data = Aramex::createPickup([
                'name' => $request->name,
                'cell_phone' => $request->cell_phone,
                'phone' => $request->phone,
                'email' => $request->email,
                'city' => $request->city,
                'country_code' => $request->country_code,
                'zip_code'=> $request->zip_code,
                'line1' => $request->line1,
                'line2' => $request->line2,
                'line3' => $request->line3,
                'pickup_date' => $request->pickup_date, // time() + 45000
                'ready_time' => $request->ready_time, // time()  + 43000
                'last_pickup_time' => $request->last_pickup_time, //time() +  45000
                'closing_time' => $request->closing_time, //time()  + 45000
                'status' => $request->status,
                'pickup_location' => $request->pickup_location,
                'weight' => $request->weight,
                'volume' => $request->volume
            ]);

            // extracting GUID
            if (!$data->error) {
                $pickupid = $data->pickupID;
                return $guid = $data->pickupGUID;

            }
        }

    }

    public function createshipment(Request $request) {

        // validate addresses and skipping struggling with users' inputs
        // thats not compatible with Aramex's end.
        $validateAddress = Aramex::validateAddress([
            'line_1'=> $request->line1, // optional (Passing it is recommended)
            'line_2'=> $request->line2, // optional
            'line_3'=> $request->line3, // optional
            'country_code' => $request->country_code,
            'city' => $request->city,
        ]);

        if (!$validateAddress->HasErrors) {
            $callResponse = Aramex::createShipment([
                'shipper' => [
                    'name' => $request->shipperName,
                    'email' => $request->shipperEmail,
                    'phone'      => $request->shipperPhone,
                    'cell_phone' => $request->shipperCell_phone,
                    'country_code' => $request->shipperCountry_code,
                    'city' => $request->shipperCity,
                    'zip_code' => $request->shipperZip_code,
                    'line1' => $request->shipperLine1,
                    'line2' => $request->shipperLine2,
                    'line3' => $request->shipperLine3,
                ],
                'consignee' => [
                    'name' => $request->consigneeName,
                    'email' => $request->consigneeEmail,
                    'phone'      => $request->consigneePhone,
                    'cell_phone' => $request->consigneeCell_phone,
                    'country_code' => $request->consigneeCountry_code,
                    'city' => $request->consigneeCity,
                    'zip_code' => $request->consigneeZip_code,
                    'line1' => $request->consigneeLine1,
                    'line2' => $request->consigneeLine2,
                    'line3' => $request->consigneeLine3,
                ],
                'shipping_date_time' => $request->shipping_date_time, // time() + 50000,
                'due_date' => $request->due_date, // time() + 60000,
                'comments' => $request->comments,
                'pickup_location' => $request->pickup_location,
                // 'pickup_guid' => $guid,
                'weight' => $request->weight,
                'number_of_pieces' => $request->number_of_pieces,
                'description' => $request->description,
            ]);
            if (!empty($callResponse->error))
            {
                foreach ($callResponse->errors as $errorObject) {
                  handleError($errorObject->Code, $errorObject->Message);
                }
            }
            else {
              // extract your data here, for example
               return $shipmentId = $callResponse->Shipments->ProcessedShipment->ID;
              // $labelUrl = $response->Shipments->ProcessedShipment->ShipmentLabel->LabelURL;
            }
        }

    }
    // Calculate Rate API is used to get shipment pricing and details before you ship it.
    public function calculaterate(Request $request) {
        $originAddress = [
            'line_1' => $request->line_1,
            'city' => $request->city, // 'Amman',
            'country_code' => $request->country_code
        ];
        $destinationAddress = [
            'line_1' => $request->line_1,
            'city' => $request->city,
            'country_code' => $request->country_code
        ];
        $shipmentDetails = [
            'weight' => $request->weight, // KG
            'number_of_pieces' => $request->number_of_pieces,
            'payment_type' => $request->payment_type, // if u don't pass it, it will take the config default value
            'product_group' => $request->product_group, // if u don't pass it, it will take the config default value
            'product_type' => $request->product_type, // if u don't pass it, it will take the config default value
            'height' => $request->height, // CM
            'width' => $request->width,  // CM
            'length' => $request->length  // CM
        ];
        $shipmentDetails = [
            'weight' => $request->weight, // KG
            'number_of_pieces' => $request->number_of_pieces,
        ];
        $currency = $request->currency;
        $data = Aramex::calculateRate($originAddress, $destinationAddress , $shipmentDetails , $currency);
        if(!$data->error){
          dd($data);

          // data content
          //{
          //  "Transaction":{
          //     "Reference1":"",
          //     "Reference2":"",
          //     "Reference3":"",
          //     "Reference4":"",
          //     "Reference5":null
          //  },
          //  "Notifications":{

          //  },
          //  "HasErrors":false,
          //  "TotalAmount":{
          //     "CurrencyCode":"USD",
          //     "Value":1004.74
          //  },
          //  "RateDetails":{
          //     "Amount":312.34,
          //     "OtherAmount1":0,
          //     "OtherAmount2":0,
          //     "OtherAmount3":78.08,
          //     "OtherAmount4":0,
          //     "OtherAmount5":475.73,
          //     "TotalAmountBeforeTax":866.15,
          //     "TaxAmount":138.59
          //  }
          //}
        }
        else{
          // handle $data->errors
        }
    }
    // This service show the detailed updates on the shipments you created.
    public function trackshipments($shipments) {
        // shipments is an array consist of shipments ids
        $data = Aramex::trackShipments($shipments);
        if (!$data->error){
          return $data->TrackingResults;
        }
        else {
        // handle error
        }
    }
    //Fetching Aramex's Countries that is supported by Aramex and stored in their database.
    //You can either get all countries or get specific country information
    // by passing country code as an optional parameter.
    public function fetchcountries(Request $request) {
        if ($request->countryCode) {
            $data = Aramex::fetchCountries($request->countryCode);
        } else {
            $data = Aramex::fetchCountries();
        }
        if (!$data->error){
          return $data->Countries;
        }
        else {
        // handle error
        }
    }
}
