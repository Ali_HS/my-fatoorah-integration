<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('payment', function(){
    return view('payment');
});

Route::group(['namespace' => 'Api', 'prefix' => 'api/'], function(){

    Route::post('payment', 'PaymentController@pay')->name('payment');

    Route::post('status/payment', 'PaymentController@status')->name('payment.status');

    Route::group(['namespance' => 'Shipping'], function(){
        Route::group(['prefix' => 'aramex', 'as' => 'aramex.'], function(){
            Route::post('pickup', 'AramexController@createpickup')->name('pickup');

            Route::post('shipment', 'AramexController@createshipment')->name('shipment.create');

            Route::post('rate', 'AramexController@calculaterate')->name('shipment.calculate.rate');

            Route::post('track/shipment', 'AramexController@trackshipments')->name('shipment.track');

            Route::post('countries', 'AramexController@fetchcountries')->name('aramex.countries');
        });

        Route::post('dhl/shipment', 'DhlController@createshipment')->name('dhl.shipment.create');

        Route::post('ups/shipment', 'UpsController@createshipment')->name('ups.shipment.create');

        Route::post('smsa/shipment', 'SmsaController@createshipment')->name('smsa.shipment.create');
    });

});

Route::get('/', function () {
    return view('welcome');
});
